FROM openjdk
COPY build/libs/hello-0.1.0.jar /hello.jar
EXPOSE 9090
ENTRYPOINT [ "java", "-jar", "/hello.jar" ]
