docker image pull jenkins/jenkins:lts
docker image pull portainer/portainer 


docker container run --detach --name jenkins --publish 8081:8080 --publish 50000:50000 --volume jenkins_home:/var/jenkins_home jenkins/jenkins:lts

docker container run --detach --publish 9000:9000 --publish 8000:8000 --name portainer --restart always --volume /var/run/docker.sock:/var/run/docker.sock --volume portainer_home:/data portainer/portainer