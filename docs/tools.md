Tools

##The following tools are required to be installed to your machine to complete the technical set up process:###

##Chocolatey (win)##

###Chocolatey is a package manager and installer for software packages, for installing software on Windows machines, to simplify the process from the user perspective.###


##Homebrew (mac)##

###Homebrew is a free and open-source software package management system that simplifies the installation of software on Apple's macOS operating system and Linux.###

##Putty##

###PuTTY is an open-source terminal emulator, serial console and network file transfer application. ###


Git
Github
Docker Hub

Visual Studio Code
VS Extensions: 

InteliJ 
IJ Extensions:

Selenium
Quantum

Test Containers - jenkins portainer bitbucket
Gradle
Maven 
SourceTree
Tortoise

Java (recent version 8+)

Cross platform Powershell

NB: PG states "Autocomplete on command line would be useful"

Proprietry Tools already included in Windows:

//Command Line