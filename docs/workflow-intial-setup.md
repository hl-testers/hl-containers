# Workflow

Instructions for class.

## Initial containers setup (ubuntu)

1. login to vm with ssh
2. make src directory
3. clone class repos from bitbucket
4. run the setup script 

### login to vm with ssh
    
    Log in with putty

### make src directory

```
    mkdir src
```

### clone class repos from bitbucket

```
    git clone https://verstructor@bitbucket.org/hl-testers/hl-containers.git
```
### run the setup script 

```
    cd src/hl-container/tools
    bash container-setup-ubuntu.sh
```